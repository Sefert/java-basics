
DROP TABLE IF EXISTS order_rows;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS USERS;
DROP TABLE IF EXISTS AUTHORITIES;

DROP SEQUENCE IF EXISTS seq1;

CREATE SEQUENCE seq1 AS INTEGER START WITH 1;

CREATE TABLE orders (
   id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
   order_number VARCHAR(255) NOT NULL
);

CREATE TABLE order_rows (
   item_name VARCHAR(255) NOT NULL,
   quantity INT NOT NULL,
   price NUMERIC NOT NULL,
   orders_id BIGINT NOT NULL REFERENCES orders(id) ON DELETE CASCADE
);

CREATE TABLE USERS (
    username VARCHAR(255) NOT NULL PRIMARY KEY,
    password VARCHAR(255) NOT NULL,
    enabled BOOLEAN NOT NULL,
    first_name VARCHAR(255) NOT NULL
);

CREATE TABLE AUTHORITIES (
    username VARCHAR(50) NOT NULL,
    authority VARCHAR(50) NOT NULL,
    FOREIGN KEY (username) REFERENCES USERS
    ON DELETE CASCADE
);

INSERT INTO USERS(username, password, enabled, first_name) VALUES
    ('user','$2y$10$mfwR46IwQwQXPmKth76LxOiGf7MO1IAdc/ly2eqF5rss9CqZMEjEa',true,'Berit'),
    ('admin','$2y$10$PotyQ1kTTEvz5Zwy3bvghui/UHKhm9vMtwB3j4s74.QXLCv/Caqmu',true,'Marko');

INSERT INTO AUTHORITIES(username, authority) VALUES
    ((SELECT username from USERS WHERE USERS.first_name ='Berit'),'USER'),
    ((SELECT username from USERS WHERE USERS.first_name ='Marko'),'ROLE_ADMIN');