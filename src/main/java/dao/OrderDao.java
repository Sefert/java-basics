package dao;

import model.Order;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Component
public class OrderDao{

    @PersistenceContext
    private EntityManager em;

    public List<Order> getOrderList(){
        return em.createQuery("SELECT o FROM Order o", Order.class).getResultList();
    }

    @Transactional
    public Order saveOrder(Order order){
        if (order.getId() == null) {
            em.persist(order);
        } else {
            em.merge(order);
        }
        return order;
    }

    public Order getOrderById(Long id){
        return em.find(Order.class, id);
    }

    @Transactional
    public void deleteOrder(Long id){
        em.createQuery("DELETE FROM Order WHERE id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }


    @Transactional
    public void deleteOrders(){
        em.createQuery("DELETE FROM Order").executeUpdate();
    }
}
