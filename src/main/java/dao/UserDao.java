package dao;

import model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Component
public class UserDao{

    @PersistenceContext
    private EntityManager em;

    //TODO: Validate existing users
    @Transactional
    public User save(User user) {
        if (user.getFirstName() == null) {
            em.persist(user);
        } else {
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            em.merge(user);
        }
        return user;
    }

    public User findByUsername(String username) {
        return em.find(User.class, username);
    }

    public List<User> getUserList(){
        return em.createQuery("SELECT u FROM User u", User.class).getResultList();
    }
}
