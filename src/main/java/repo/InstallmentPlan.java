package repo;

import model.Installment;
import model.Order;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface InstallmentPlan {
    List<Installment> generateInstallments(Order order, Map<String, String> params) throws ParseException;
}
