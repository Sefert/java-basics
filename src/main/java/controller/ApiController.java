package controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {

    //TODO:return actual version
    @GetMapping("version")
    public String getVersion() {
        return "v1.0";
    }
}
