package controller;

import dao.UserDao;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserDao userDao;

    @PostMapping("register")
    public User save(@RequestBody @Valid User user) {
        return userDao.save(user);
    }

    @GetMapping("users/{userName}")
    @PreAuthorize("#userName == authentication.name or hasAuthority('ROLE_ADMIN')")
    public User getUserByName(@PathVariable String userName) {
        return userDao.findByUsername(userName);
    }

    @GetMapping("users")
    public List<User> getUsers() {
        return userDao.getUserList();
    }
}
