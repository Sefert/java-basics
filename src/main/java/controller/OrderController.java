package controller;

import dao.OrderDao;
import service.InstallmentPlanImpl;
import model.Installment;
import model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class OrderController {

    @Autowired
    private OrderDao orderDao;

    @GetMapping("orders")
    public List<Order> getOrders() {
        return orderDao.getOrderList();
    }

    @PostMapping("orders")
    public Order save(@RequestBody @Valid Order order) {
        return orderDao.saveOrder(order);
    }

    @GetMapping("orders/{id}")
    public Order getOrder(@PathVariable Long id){
        return orderDao.getOrderById(id);
    }

    @GetMapping("orders/{id}/installments")
    public List<Installment> getInstallment(@PathVariable Long id, @RequestParam(required = false) Map<String, String> params){
         return new InstallmentPlanImpl().generateInstallments(orderDao.getOrderById(id), params);
    }

    @DeleteMapping("orders/{id}")
    public void delete(@PathVariable Long id){
        orderDao.deleteOrder(id);
    }

    @DeleteMapping("orders")
    public void deleteAll(){
        orderDao.deleteOrders();
    }
}
