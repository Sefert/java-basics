package service;

import lombok.Setter;
import model.Installment;
import model.Order;
import model.OrderRow;
import org.springframework.stereotype.Service;
import repo.InstallmentPlan;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Service
public class InstallmentPlanImpl implements InstallmentPlan {

    @Setter
    private Long minAllowedAmount = 3L;

    private List<Installment> installments;
    private LocalDate startDate;
    private LocalDate endDate;

    public InstallmentPlanImpl(){
        this.installments = new ArrayList<>();
    }

    @Override
    public List<Installment> generateInstallments(Order order, Map<String, String> period){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        this.startDate = LocalDate.parse(period.get("start"), formatter);
        this.endDate = LocalDate.parse(period.get("end"), formatter);

        BigDecimal orderSum = orderSum(order);

        changeEndDateAccordingToMinAllowedAmount(orderSum);
        generateInstallmentPeriod(orderSum);

        return this.installments;
    }

    private BigDecimal orderSum(Order order){

        //https://stackoverflow.com/questions/22635945/adding-up-bigdecimals-using-streams
        Function<OrderRow, BigDecimal> totalMapper = orderRow -> orderRow.getPrice().multiply(BigDecimal.valueOf(orderRow.getQuantity()));
        return order.getOrderRows().stream()
                .map(totalMapper)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private void changeEndDateAccordingToMinAllowedAmount(BigDecimal amount){

        Long requestedPeriod = getPeriodInMonths();
        Long conditionalPeriod = amount.longValue()/this.minAllowedAmount;

        if (conditionalPeriod < requestedPeriod){
            this.endDate = this.endDate.minusMonths(requestedPeriod - conditionalPeriod);
        }
    }

    private Long getPeriodInMonths(){
        return ChronoUnit.MONTHS.between(this.startDate.withDayOfMonth(1), this.endDate.withDayOfMonth(1)) + 1L;
    }

    //TODO: Add support for not full number modulo
    private void generateInstallmentPeriod(BigDecimal amount){

        Long paymentPeriod = getPeriodInMonths();
        BigDecimal amountRemainder = amount.remainder(BigDecimal.valueOf(paymentPeriod)).setScale(0, RoundingMode.DOWN);
        BigDecimal partialPayment = amount.subtract(amountRemainder).divide(BigDecimal.valueOf(paymentPeriod), RoundingMode.DOWN).setScale(0, RoundingMode.DOWN);

        for (int months = 0; months < paymentPeriod; months++) {
            if (months == 0){
                this.installments.add(new Installment(partialPayment,startDate));
            } else if ((Long.valueOf(months) + amountRemainder.longValue()) >= paymentPeriod){
                this.installments.add(new Installment(partialPayment.add(BigDecimal.ONE),startDate.withDayOfMonth(1).plusMonths(months)));
            } else {
                this.installments.add(new Installment(partialPayment,startDate.withDayOfMonth(1).plusMonths(months)));
            }
        }
    }
}
