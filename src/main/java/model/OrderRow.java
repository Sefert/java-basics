package model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Embeddable
@Table(name = "order_rows")
public class OrderRow{

    @Column(name = "item_name")
    private String itemName;

    @NotNull
    @Min(1)
    @Column(name = "quantity")
    private int quantity;

    @NotNull
    @DecimalMin("0.01")
    @Column(name = "price")
    private BigDecimal price;
}
